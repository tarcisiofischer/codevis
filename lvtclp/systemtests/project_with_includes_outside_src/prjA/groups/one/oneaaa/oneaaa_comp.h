#ifndef DIAGRAM_SERVER_ONEAAA_COMP_H
#define DIAGRAM_SERVER_ONEAAA_COMP_H

#include <twoaaa_comp.h>

class A {
  public:
    void foo() const;
};

#endif // DIAGRAM_SERVER_ONEAAA_COMP_H
